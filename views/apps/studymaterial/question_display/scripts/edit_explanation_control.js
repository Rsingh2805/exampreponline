// getdata function for converting explanation data to proper format
		function getData(element){
			var data = element.map(function(i, element){
				if($(this).data('type')=="simple"){
					var myobj = new Object();
					myobj.type = 'simple';
					myobj.value = $(this).val();
					return myobj;
				}else if($(this).data('type')=="codemirror"){
					var myobj = new Object();
					myobj.type = 'codemirror';
					myobj.value = $(this).next('.CodeMirror')[0].CodeMirror.getValue();
					return myobj;
				}else if($(this).data('type')=="ckeditor"){
					var myobj = new Object();
					myobj.type = 'ckeditor';
					myobj.value = $(this).val();
					return myobj;
				}
			}).get();
			return data;
		}


// on click of edit explanation button
		$('#wrapper').on("click",".edit_explanation", function(e){
			e.preventDefault();
			var parent = $(this).parent('div');
			// console.log(parent);

			// make textareas and codemirror editable
			parent.find("textarea").prop('readonly', false);

			var check = parent.find(".code").next('.CodeMirror').each(function(){
				var codemirror = $(this)[0].CodeMirror;
				codemirror.setOption("readOnly", false)
			});
			
			// change edit to save
			$(this).text('Save');

			// change class of the button
			$(this).attr("class", "save_explanation");
		});


// on click of save explanation button
		$('#wrapper').on("click",".save_explanation", function(e){
			e.preventDefault();
			var parent = $(this).parent('div');
			var form = parent.find(".explanation_form").serialize();
			var edit_explanation_form = form;
			// console.log(edit_explanation_form);
			var obj = getData(parent.find("textarea"));
			var explanation = JSON.stringify(obj);
			// console.log(parent);
			id = $(this).parent('div').parent('div').parent('div').parent('div').parent('div').parent('li').parent('div').eq(0).attr('id');
			// console.log(id);
			var data='req_type=edit'+'&'+edit_explanation_form+'&explanation='+explanation+'&ques_id='+id;


			// ajax request to form
			var explanation_updated = ajaxSubmitForm(parent.find(".explanation_form")[0], data);
			// console.log(explanation_updated);


			// set back textareas to readonly
			parent.find("textarea").prop('readonly', true);
			var check = parent.find(".code").next('.CodeMirror').each(function(){
				var codemirror = $(this)[0].CodeMirror;
				// console.log(codemirror);
				codemirror.setOption("readOnly", true)
			});
			$(this).text('Edit');
			$(this).attr("class", "edit_explanation");
		});
