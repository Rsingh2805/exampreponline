function set_explanation(explanation_array, explanation_id, explanation_form){
		var current_explanation = JSON.parse(explanation_array[explanation_id].explanation);
		// console.log(current_explanation);
		var explanation_html='';
		for (i=0; i<current_explanation.length; i++){
			if (current_explanation[i].type=='simple'){
	            // data no attribute to keep track of which no of textarea it is so that we can easily get reference as in line 97
	            explanation_html+= '<textarea class="form-control" data-type="simple" name="simple[]" readonly=true>'+current_explanation[i].value +'</textarea>';
	        }else if(current_explanation[i].type=='codemirror'){
	        	explanation_html+= '<textarea class="code" data-type="codemirror">'+ current_explanation[i].value +'</textarea>';
	        };
	    };
	    // console.log(explanation_html);
	    explanation_form.html(explanation_html);
	    explanation_form.find('.code').each(function(){
  					// console.log($(this));
  					var x = $(this).data('no');
            // editor x will keep track of the codemirrors for us so that we can just use index when getting the data in getdata function
  					editor[x] = CodeMirror.fromTextArea($(this)[0], {
  						mode: "text/x-csharp",
  						// lineNumbers: true,
  						// styleActiveLine: true,
  						matchBrackets: true,
  						theme: "monokai",
  						height: "auto",
              mode: "javascript",
  					});
  				})
}

var explanations = {};
var explanation_html = '';
$('.show_explanation').on("click", function(e){
	e.preventDefault();
	// console.log(jQuery.isEmptyObject(explanations));
	var id= $(this).parent('h5').parent('div').parent('div').parent('div').next('.report').attr('id');
	var form = $(this).parent('h5').parent('div').parent('div').find('form');
	if(jQuery.isEmptyObject(explanations)){
		$.ajax({
			type: 'post',
			url: './?controller=questions&action=explanation_handler',
			data: 'req_type=view',
			success:function(htm){
				$.each(JSON.parse(htm).records, function(index, value){
					explanations[value.ques_id] = value;
				});

				set_explanation(explanations, id, form);
			},
			error:function(htm){

			},
		});
	}else{
		// console.log(explanations);
		set_explanation(explanations, id, form);

	}

	// $(this).parent('h5').parent('div').parent('div').find('form')[0].html(explanation_html);
	// console.log(form[0]);

})