	// this editor variable holds the array of codemirror editors so that you can retrieve their value later
  var editor = {};

// ready makes sure script loads after the html for modal is loaded
$('#reportModal').ready(function(){

// controlling the content in the modal
// on shown bs is used whenever modal is shown
	$('#reportModal').on('show.bs.modal', function (event) {

		var button = $(event.relatedTarget);
  	// console.log(button); // Button that triggered the modal
  	var id = button.attr('id');
  	var data='req_type=view'+'&id='+id;

// ajax request to get content
  	$.ajax({
  		type: 'post',
  		url: './?controller=questions&action=question_handler',
  		data: data,
  		success:function(htm){
                              // console.log(htm);
  			// console.log(JSON.parse(htm).records[0]);

        // question var contains the question title
  			var question = JSON.parse(JSON.parse(htm).records[0].title);
  			// console.log(question);

        // solution var contains the solution part
  			var solution = JSON.parse(JSON.parse(htm).records[0].solution);
  			// console.log(solution); 	

        // question text will contain html content for the question part in modal
  			var question_text = '';
  			for (i=0; i<question.length; i++){
  				if (question[i].type=='simple'){
            // data no attribute to keep track of which no of textarea it is so that we can easily get reference as in line 97
  					question_text+= '<textarea class="form-control" data-type="simple" data-no="'+i+'" name="question_edit[]">'+question[i].value +'</textarea>';
  				}else if(question[i].type=='codemirror'){
  					question_text+= '<textarea class="code" data-type="codemirror" data-no="'+i+'" name="question_edit[]">'+ question[i].value +'</textarea>';
  				};
  			};
  			// console.log(question_text);
  			$('.question_div').html(question_text);


        // converting textareas to codemirror
  			$('.question_div').ready(function(){
  				$('.question_div').find('.code').each(function(){
  					// console.log($(this));
  					var x = $(this).data('no');
            // editor x will keep track of the codemirrors for us so that we can just use index when getting the data in getdata function
  					editor[x] = CodeMirror.fromTextArea($(this)[0], {
  						mode: "text/x-csharp",
  						// lineNumbers: true,
  						// styleActiveLine: true,
  						matchBrackets: true,
  						theme: "monokai",
  						height: "auto",
              mode: "javascript",
  					});
  				})  				
  			});


        // putting option value in place
  			$('#option1').text(solution.option1);
  			$('#option2').text(solution.option2);
  			$('#option3').text(solution.option3);
  			$('#option4').text(solution.option4);

        // putting option value in front of modal
  			$('#val1').text(solution.option1);
  			$('#val2').text(solution.option2);
  			$('#val3').text(solution.option3);
  			$('#val4').text(solution.option4);
  			$('input:radio[name=option]').filter('[value='+solution.option+']').prop('checked', true);

        // setting other attributes
  			$('#ques_id').val(id);
  			$('#ques_id').attr('readOnly', true);
  		},
  		error : function(htm){
  			// console.log(htm);

  		}
  	});
  	
  });

  // controlling the submission of modal report
	$('#submit_report').on("click", function(e){
		e.preventDefault();
		var element = $("textarea[name='question_edit[]']");


    // convert question data to proper format
		var data = element.map(function(i, element){
			// console.log($(element).data('type'));
			if($(this).data('type')=="simple"){
				var myobj = new Object();
				myobj.type = 'simple';
				myobj.value = $(this).val();
				return myobj;
			}else if($(this).data('type')=="codemirror"){
				var myobj = new Object();
				myobj.type = 'codemirror';
        // as we simply used editor x we can just use the index to get the particular codemirror instance
				myobj.value = editor[i].getValue();
				return myobj;
			}else if($(this).data('type')=="ckeditor"){
				var myobj = new Object();
				myobj.type = 'ckeditor';
				myobj.value = $(this).val();
				return myobj;
			}
		}).get();
		// console.log(data);


    // controlling solution options
		var new_solution = new Object();
		new_solution.option1 = $('#option1').val();
		new_solution.option2 = $('#option2').val();
		new_solution.option3 = $('#option3').val();
		new_solution.option4 = $('#option4').val();
		new_solution.option = $('input:radio[name=option]:checked').val();
		id=$('#ques_id').val()
		// console.log(JSON.stringify(new_solution));
		
    // ajax request
    var data='req_type=edit'+'&'+'&solution='+JSON.stringify(new_solution)+'&id='+id+'&question='+JSON.stringify(data);
		var report_result = ajaxSubmitForm($('#reportModal').find(".report_form")[0], data);
		// console.log(JSON.parse(report_result));

    // controlling the ajax result
		report_result = JSON.parse(report_result);
		if(report_result.status=="OK"){
			$('#dialog_msg').html('<div class="notify alert alert-success"><b>Success: </b>Changed Successfully</div>');
			setTimeout(function(){
        $('.notify').hide(); 
        $('.modal_close_btn').trigger('click');
      }, 3000);
			$()
		}else{
			$('#dialog_msg').html('<div class="notify alert alert-danger"><b>Error: </b>Something went wrong. Please try again</div>');
			setTimeout(function(){ $('.notify').hide(); }, 3000);
		}
	})

});