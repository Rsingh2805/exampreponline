<?php include 'views/libraries/question_type.php'; ?>
<?php include 'views/libraries/codemirror/codemirror.php' ?>
<?php include 'views/libraries/ckeditor/ckeditor.php' ?>
<!-- contains the codemirror imports -->
<?php $header = $codemirror_header; ?>
<!-- contains the ckeditor imports -->
<?php $header.= $ckeditor_header; ?>
<?php $header.= $header_index; ?>
<?php
include 'views/templates/header.php';
$title = "Add a question";
?>
<script src="views/scripts/js_apis/post_submit.js"></script>
<?php $counter=0; ?>
<div id="wrapper">
	<?php foreach($questions as $question): ?>
		<?php
// question display
		switch($question->type){
			case "textual":
			$obj = new textual($question->title, $question->solution, $question->explanation, $question->id);
			break;

			case "mcq":
			$obj = new mcq($question->title, $question->solution, $question->id);
			break;

			case "logical":
			$obj = new logical($question->title, $question->solution, $question->explanation, $question->id);
			break;

			case "other":
			$obj = new other($question->title, $question->solution, $question->explanation, $question->id);
			break;
		}
		?>
		<?php echo '<div id="'.$question->id.'">'; ?>
			<?php echo $obj->show(); ?>
			<?php echo '</div>'; ?>
			<?php $counter++; ?>
		<?php endforeach; ?>
	</div>
	<?php echo $codemirror_footer; ?>




	<!-- Modal for report -->
	<div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div id="dialog_msg"></div>
				<div class="modal-header">
					<h5 class="modal-title" id="reportModalLabel">Report</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<form class="report_form" action="?controller=questions&action=question_handler">
						<input type="hidden" name="id" id="ques_id" />
						<div class="accordion" role="tablist">
							<div class="card">
								<div class="card-header" role="tab" id="headingOne">
									<h5 class="mb-0">
										<a data-toggle="collapse" href="#collapseOne" role="button" aria-expanded="true" aria-controls="collapseOne">
											Question:
										</a>
									</h5>
								</div>

								<div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
									<div class="card-body question_div">
										
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" role="tab" id="headingTwo">
									<h5 class="mb-0">
										<a class="collapsed" data-toggle="collapse" href="#collapseTwo" role="button" aria-expanded="false" aria-controls="collapseTwo">
											Options:
										</a>
									</h5>
								</div>
								<div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
									<div class="card-body">
										<div class="form-group">
											<div class="row container">
												<textarea style="margin:3px;" class="form-control col-md-4" id="option1"></textarea>
												<textarea style="margin:3px;" class="form-control col-md-4" id="option2"></textarea>
												<textarea style="margin:3px;" class="form-control col-md-4" id="option3"></textarea>
												<textarea style="margin:3px;" class="form-control col-md-4" id="option4"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" role="tab" id="headingThree">
									<h5 class="mb-0">
										<a class="collapsed" data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">
											Correct Option:
										</a>
									</h5>
								</div>
								<div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
									<div class="card-body">
										<div class="form-group">
											<div class="row container">
												<input type="radio" name="option" value="a" required>
												<textarea style="margin:3px;" class="form-control col-md-4" id="val1" readOnly></textarea>
												<input type="radio" name="option" value="b" required>
												<textarea style="margin:3px;" class="form-control col-md-4" id="val2" readonly=""></textarea>
											</div>
											<div class="row container">
												<input type="radio" name="option" value="c" required>
												<textarea style="margin:3px;" class="form-control col-md-4" id="val3" readonly=""></textarea>
												<input type="radio" name="option" value="d" required>
												<textarea style="margin:3px;" class="form-control col-md-4" id="val4" readonly=""></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>	

						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="modal_close_btn btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" id="submit_report" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</div>
		</div>

<script src="views/apps/studymaterial/question_display/scripts/edit_explanation_control.js"></script>
<script src="views/apps/studymaterial/question_display/scripts/modal_control.js"></script>
<script src="views/apps/studymaterial/question_display/scripts/display_explanation.js"></script>

<?php include 'views/templates/footer.php' ?>