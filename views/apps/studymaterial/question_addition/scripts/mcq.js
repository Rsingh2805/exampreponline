
   //to reset the form on submission 
   function resetForm(element){
    element[0].reset();
    for(var k=x; k>=0; k--){
      $('.ques'+k).children(".remove_field").trigger("click");
    }
    for(var k=y; k>=0; k--){
      $('.exp'+k).children(".remove_field").trigger("click");
    }
    $(add_button).trigger("click");
    $(add_button_2).trigger("click");

  }
  // this script is for the question section


    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var codemirror_add = $(".add_codemirror_button");
    var ckeditor_add = $(".add_ckeditor_button");
    var x = 0; //initlal text box count
    var editor = {};
    $(add_button).click(function(e){ //on add input button click
      e.preventDefault();
        if(x < max_fields){ //max input box allowed
            $(wrapper).append('<div class="ques'+x+'"><textarea type="text" class="form-control" data-type="simple" form-control-sm" name="mytext[]" rows="4" required/><a href="#" class="remove_field">Remove</a></div>'); //add input box
          }
            x++; //text box increment        
          });
    $(codemirror_add).click(function(e){ //on add input button click
      e.preventDefault();
        if(x < max_fields){ //max input box allowed
            $(wrapper).append('<div class="ques'+x+'"><textarea required class="code" data-type="codemirror" name="mytext[]"></textarea><a href="#" class="remove_field">Remove</a></div>'); //add input box
          }
          // convert the above editor to a codemirror
          editor[x] = CodeMirror.fromTextArea($('.code:last')[0], {
            mode: "text/x-csharp",
            lineNumbers: true,
            styleActiveLine: true,
            matchBrackets: true,
            theme: "monokai",
            height: "auto",
          });
        x++; //text box increment
      });
    $(ckeditor_add).click(function(e){ //on add input button click
      e.preventDefault();
        if(x < max_fields){ //max input box allowed
          $(wrapper).append('<div class="ques'+x+'"><textarea required class="ckeditor" data-type="ckeditor" name="mytext[]"></textarea><a href="#" class="remove_field">Remove</a></div>');
          $('.ckeditor:last').ready(function(){
            editor[x] = $('.ckeditor:last').ckeditor();
          });
             //add input box
           }
                     x++; //text box increment
                   });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); $(this).parent('div').remove(); x--;
    });

// this script is for question explanation

    var max_fields      = 10; //maximum input boxes allowed
    var wrapper_2         = $(".input_fields_wrap_2"); //Fields wrapper
    var add_button_2      = $(".add_field_button_2"); //Add button ID
    var codemirror_add_2 = $(".add_codemirror_button_2");
    var ckeditor_add_2 = $(".add_ckeditor_button_2");
    var y = 0; //initlal text box count
    var editor = {};
    $(add_button_2).click(function(e){ //on add input button click
      e.preventDefault();
        if(y < max_fields){ //max input box allowed
            $(wrapper_2).append('<div class="exp'+x+'"><textarea required type="text" class="form-control" data-type="simple" form-control-sm" name="myexplanation[]" rows="4"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
          }
            y++; //text box increment        
          });
    $(codemirror_add_2).click(function(e){ //on add input button click
      e.preventDefault();
        if(y < max_fields){ //max input box allowed
            $(wrapper_2).append('<div class="exp'+x+'"><textarea required class="code_exp" data-type="codemirror" name="myexplanation[]"></textarea><a href="#" class="remove_field">Remove</a></div>'); //add input box
          }

          editor[y] = CodeMirror.fromTextArea($('.code_exp:last')[0], {
            mode: "text/x-csharp",
            lineNumbers: true,
            styleActiveLine: true,
            matchBrackets: true,
            theme: "monokai",
            height: "auto",
          });
        y++; //text box increment
      });
    $(ckeditor_add_2).click(function(e){ //on add input button click
      e.preventDefault();
        if(y < max_fields){ //max input box allowed
          $(wrapper).append('<div class="exp'+x+'"><textarea required class="ckeditor_exp" data-type="ckeditor" name="myexplanation[]"></textarea><a href="#" class="remove_field">Remove</a></div>');
          $('.ckeditor:last').ready(function(){
            editor[y] = $('.ckeditor_exp:last').ckeditor();
          });
             //add input box
           }
                     x++; //text box increment
                   });

    $(wrapper_2).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); $(this).parent('div').remove(); y--;
    });

    $(add_button_2).trigger("click");
    $(add_button).trigger("click");
