// for typeahead autocomplete
var content="";

// this function will return the data from an array of elemnts like
// the result ll be an array with each object representing a text box with 2 attributes, type and value
// this is how the data will be stored in the db
function getData(element){
	var data = element.map(function(i, element){
		// console.log($(element).data('type'));
		if($(this).data('type')=="simple"){
			var myobj = new Object();
			myobj.type = 'simple';
			myobj.value = $(this).val();
			return myobj;
		}else if($(this).data('type')=="codemirror"){
			var myobj = new Object();
			myobj.type = 'codemirror';
			myobj.value = editor[i].getValue();
			return myobj;
		}else if($(this).data('type')=="ckeditor"){
			var myobj = new Object();
			myobj.type = 'ckeditor';
			myobj.value = $(this).val();
			return myobj;
		}



	}).get();
	return data;
}



$(document).ready(function () {
//   $('#question_text').typeahead({
//    source: function (query, result) {
//     $.ajax({
//       url: "../data_handlers/auto_complete_handler.php",
//       data: 'query=' + query,            
//       dataType: "json",
//       type: "POST",
//       success: function (data) {
//         result($.map(data, function (item) {
//          return item;
//        }));
//       }
//     });
//   }
// });



// for inporting content of question submission on add question.php page
$( "#question_type" ).change(function () {
	content = $('#question_type option:selected').val();
	$( "#tab-content-import" ).load('views/apps/studymaterial/question_addition/views/question_types/' + content+'.php');  
});




$('#add_button').click(function(e){

	// var to check if error exists
	var error='';

	e.preventDefault();
	if(content=='textual'){
		var question_solution_code = editor.getValue();
	}else if(content=='logical'){
		var abc = CKEDITOR.instances['question_explanation'].getData();
		document.getElementById("question_explanation").value = abc;
	}else if(content=='mcq'){
		var option = $('[name=option]:checked').val();
		var option1 = $('[name=option1]').val();
		var option2 = $('[name=option2]').val();
		var option3 = $('[name=option3]').val();
		var option4 = $('[name=option4]').val();
		
		var solutionObject = new Object();
		solutionObject.option = option;
		solutionObject.option1 = option1;
		solutionObject.option2 = option2;
		solutionObject.option3 = option3;
		solutionObject.option4 = option4;
		// console.log(option+option1+option2+option3+option4);
		if(!option){
			error = 1;
			$('#dialog_msg').html('<div class="notify alert alert-danger"><b>Error: </b>You must select an option</div>');
			setTimeout(function(){ $('.notify').hide(); }, 3000);
		}else if(option1==''){
			error = 1;
			$('#dialog_msg').html('<div class="notify alert alert-danger"><b>Error: </b>Option a. should be filled</div>');
			setTimeout(function(){ $('.notify').hide(); }, 3000);
		}else if(option2==''){
			error = 1;
			$('#dialog_msg').html('<div class="notify alert alert-danger"><b>Error: </b>Option b. should be filled</div>');
			setTimeout(function(){ $('.notify').hide(); }, 3000);
		}else if(option3==''){
			error = 1;
			$('#dialog_msg').html('<div class="notify alert alert-danger"><b>Error: </b>Option c. should be filled</div>');
			setTimeout(function(){ $('.notify').hide(); }, 3000);
		}else if(option4==''){
			error = 1;
			$('#dialog_msg').html('<div class="notify alert alert-danger"><b>Error: </b>Option d. should be filled</div>');
			setTimeout(function(){ $('.notify').hide(); }, 3000);
		}

	}else if(content=='other'){

	}
	var add_question_form = $('#add_question_form').serialize();
	if(content=='textual'){
		var data = 'req_type=add' + '&'+add_question_form+'&question_solution_code='+question_solution_code;
	}else if(content=='mcq'){
		var obj = getData($("textarea[name='mytext[]']"))
			// if($("#question_text").data('type')=="simple"){
			// 	var obj = new Object();
			// 	obj.type = 'simple';
			// 	obj.value = $("#question_text").val();
			// }else if($("#question_text").data('type')=="codemirror"){
			// 	var obj = new Object();
			// 	obj.type = 'codemirror';
			// 	console.log(editor1);
			// 	obj.value = editor1.getValue();
			// }if($("#question_text").data('type')=="ckeditor"){
			// 	var obj = new Object();
			// 	obj.type = 'ckeditor';
			// 	obj.value = $("#question_text").val();
			// }
			var queserror = 0;
			jQuery.each(obj, function(index, item){
				if (item.value == ''){
					queserror = 1;
				}else{
					queserror=0;
					return false;
				}
			});
			var question = JSON.stringify(obj);
			if(queserror==1){
				error = 1;
				$('#dialog_msg').html('<div class="notify alert alert-danger"><b>Error: </b>A Question is required</div>');
				setTimeout(function(){ $('.notify').hide(); }, 3000);
			}
			var obj2 = getData($("textarea[name='myexplanation[]']"))
			var experror = 0;
			jQuery.each(obj2, function(index, item){
				if (item.value == ''){
					experror = 1;
				}else{
					experror=0;
					return false;
				}
			});
			if(experror==1){
				error = 1;
				$('#dialog_msg').html('<div class="notify alert alert-danger"><b>Error: </b>An Explanation is required</div>');
				setTimeout(function(){ $('.notify').hide(); }, 3000);
			}
			// if($("#question_explanation").data('type')=="simple"){
			// 	var obj2 = new Object();
			// 	obj2.type = 'simple';
			// 	obj2.value = $("#question_text").val();
			// }else if($("#question_explanation").data('type')=="codemirror"){
			// 	var obj2 = new Object();
			// 	obj2.type = 'codemirror';
			// 	obj2.value = editor2.getValue();
			// }if($("#question_explanation").data('type')=="ckeditor"){
			// 	var obj2 = new Object();
			// 	obj2.type = 'ckeditor';
			// 	obj2.value = $("#question_text").val();
			// }
			var explanation = JSON.stringify(obj2);
			var solution = JSON.stringify(solutionObject);
			// console.log(obj2);
			// var get_question = $("#question_text")
			var data = 'req_type=add'+'&'+'question='+question +'&'+'explanation='+explanation+'&'+'solution='+solution+'&'+add_question_form;
			// console.log(data);
		}else if(content=='logical'){
			var data = 'req_type=add' + '&' + add_question_form;
		}else if(content=='other'){
			var get_data = getData($("textarea[name='mytext[]']"))
			// var get_data = $("textarea[name='mytext[]']")
   //            .map(function(i, element){
   //            	console.log($(element).data('type'));
   //            	if($(this).data('type')=="simple"){
   //            		var myobj = new Object();
   //            		myobj.type = 'simple';
   //            		myobj.value = $(this).val();
   //            		return myobj;
   //            	}else if($(this).data('type')=="codemirror"){
   //            		var myobj = new Object();
   //            		myobj.type = 'codemirror';
   //            		myobj.value = editor[i].getValue();
   //            		return myobj;
   //            	}else if($(this).data('type')=="ckeditor"){
   //            		var myobj = new Object();
   //            		myobj.type = 'ckeditor';
   //            		myobj.value = $(this).val();
   //            		return myobj;
   //            	}



   //            }).get();
   // console.log(JSON.stringify(get_data));
   var data = 'req_type=add' + '&'+add_question_form+'&question_solution_code='+JSON.stringify(get_data);
}
$('#refresh').attr("disabled", false);
// console.log(error);
if(!error){
	var question_add_data = ajaxSubmitForm( $('#add_question_form')[0], data );
	// console.log(htm);
	var result = JSON.parse(question_add_data);
	// console.log(result);
	htm = result.msg;
	if(htm!='0'){
		$('#dialog_msg').html('<div class="notify alert alert-danger"><b>Error: </b>Some error happened. Please try again</div>');
		setTimeout(function(){ $('.notify').hide(); }, 3000);
	}else{
		$('#dialog_msg').html('<div class="notify alert alert-success"><b>Notice: </b>Added successfully</div>');
		setTimeout(function(){ $('.notify').hide(); }, 3000);
		resetForm($('#add_question_form'));
		
	}

}
});





});



// initialization
$(document).ready(function() {
	$('#question_type')
	.val('mcq')
	.trigger('change');
});





