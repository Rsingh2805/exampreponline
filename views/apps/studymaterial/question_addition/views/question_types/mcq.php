<!-- Contains format of mcq questions for add question page -->


<!-- include the js script -->
<script src="views/apps/studymaterial/question_addition/scripts/mcq.js"></script>


<div class="mcq-editor">
  <div class="mcq-editor-textbox input_fields_wrap">
    <label for="question_text" class="font-weight-bold">Question:</label>
  </div>
  <div class = "row">
    <button class="btn btn-primary col-md-6 plain-text-button add_field_button"  type="button">Plain Text</button>
    <button class="btn btn-success col-md-6 codemirror-button add_codemirror_button" name="codemirror_add" type="button">Codemirror</button>
    <!-- <button class="btn btn-primary col-md-4 ckeditor-button add_ckeditor_button" type="button" name="ckeditor_add">CKeditor</button> -->
  </div>
</div> 
<br>
<div id= "menu2" class="container">
  <label for="question_solution" class="font-weight-bold">Solution:</label>
  <div class="row form-group">
    <div class="form-inline col-md-6">
      <input type="radio" name="option" value="a" required>&nbsp;
      <textarea name="option1" id="option1" class="form-control form-control-sm" placeholder="Enter option 1" required="true" />
    </div>
    <div class="form-inline col-md-6">
      <input type="radio" name="option" value="b">&nbsp;
      <textarea name="option2" id="option2" class="form-control form-control-sm" placeholder="Enter option 2" required="true" />
    </div>
  </div>
  <div class="row form-group">
    <div class="form-inline col-md-6">
      <input type="radio" name="option" value="c">&nbsp;
      <textarea name="option3" id="option3" class="form-control form-control-sm" placeholder="Enter option 3" required="true" />
    </div>
    <div class="form-inline col-md-6">
      <input type="radio" name="option" value="d">&nbsp;
      <textarea name="option4" id="option4" class="form-control form-control-sm" placeholder="Enter option 4" required="true" />
    </div>
  </div>

  <div class="mcq-explanation">
    <div class="mcq-explanation-textbox input_fields_wrap_2">
      <label for="question_explanation" class="font-weight-bold">Explanation:</label>
    </div>
    <div class="row">
      <button class="btn btn-primary col-md-6 add_field_button_2 " type="button">Plain Text</button>
      <button class="btn btn-success col-md-6 add_codemirror_button_2" type="button">Codemirror</button>
      <!-- <button class="btn btn-primary col-md-4 add_ckeditor_button_2" type="button"">CKeditor</button> -->
    </div>
  </div>
  <br>
</div>
