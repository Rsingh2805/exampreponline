<!-- Contains format of logical questions for add question page -->


<div class="form-group">
	<label for="question_text" class="font-weight-bold">Question:</label>
	<input type="text" name="question_text" id="question_text" class="typeahead form-control form-control-sm" placeholder="Enter the problem question here">
</div> 
<!-- <div class="container" > -->


	<div class="tab-content" >
		<div id= "menu1">
			<div class="form-group">
				<label for="question_solution" class="font-weight-bold">Solution:</label>
				<input type="text" name="add_question_page_ta" class="form-control" id="add_question_page_ta" placeholder="Enter the solution here"></textarea>
			</div>                  
		</div>



		<div class="form-group">
			<label for="question_explanation" class="font-weight-bold">Explanation: </label>
			<textarea name="question_explanation" id="question_explanation" class="form-control form-control-sm" placeholder="Enter the explanation here"></textarea>
		</div>
		<script>
			$('#question_explanation').ready(function(){

				$('#question_explanation').ckeditor();

			});
		</script>
	</div>