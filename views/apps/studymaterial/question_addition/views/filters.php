<!-- filters for question selection -->
<div class="form-group align-self-center container">
  <label for="question_subject_id" class="font-weight-bold">Select a subject:</label>
  <select name="question_subject_id" id="question_subject_id" class="form-control form-control-sm" title="question_subject_id">

    <?php foreach($subjects as $subject ): ?>
      echo '<option value="<?php echo $subject->id?>"><?php echo $subject->title?></option>';
    <?php endforeach; ?>

  </select>
</div>
<div class="form-group align-self-center container">
  <label for="question_type" class="font-weight-bold">Select a question type</label>
  <select name="question_type" id="question_type" class="form-control form-control-sm" title="question_type">
    <option value="mcq">MCQ</option>
    <!-- <option value="logical">Logical</option>
    <option value="textual" selected="true">Textual</option>
    <option value="other">Other</option> -->
  </select>
</div>



