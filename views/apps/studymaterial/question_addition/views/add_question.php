<!-- page for adding new question -->
<?php include 'views/libraries/codemirror/codemirror.php'; ?>
<?php include 'views/libraries/ckeditor/ckeditor.php'; ?>

<!-- $header will contain all the imports -->
<?php $header = $codemirror_header; ?>
<?php $header.= $ckeditor_header; ?>
<?php $title = 'Add Question' ?>
<?php
include 'views/templates/header.php';
$title = "Add a question";
// error reporting
?>
<script src="views/scripts/js_apis/post_submit.js"></script>
<!-- <script>
  $(document).ready(function(){
                                 $('.test').ready(function(){
                       $('test').ckeditor();
                     });
                               })
  
</script>
-->
<div class="container">
  <div class="row">
    <div class="col-md-4">
      <h3>Enter a new question:</h3>
    </div>
    <div class="col-md-4" id="dialog_msg"></div>
  </div>
  <form id="add_question_form" action="?controller=questions&action=question_handler" accept-charset="utf-8">

    <div class="row">
      <div class="container-fluid col-md-3 border">
        <?php include 'views/apps/studymaterial/question_addition/views/filters.php'; ?>
      </div>
      <div class= "container-fluid col-md-9 border">
        <div id="tab-content-import"></div> 

      </div>
    </div>
    <div class="row">
      <button type="submit" class="btn btn-default col-md-12" id="add_button" >Add now</button>
    </div>
  </form>
</div>
<script src = "views/apps/studymaterial/question_addition/scripts/add_question.js"></script>
<?php include 'views/templates/footer.php' ?>
