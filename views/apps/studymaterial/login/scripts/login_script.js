$(document).ready(function(){

// login page jquery handler
	$('#login_btn').click(function(){
		var data = 'r=login_data'+'&'+ $('#login_form').serialize();
		// console.log(data);
		$.ajax({
			type: 'post',
			url: './?controller=user&action=login_handler',
			data: data,
			success:function(htm){	
				if(htm=='0'){
					window.location.replace('./');
				}else if(htm=='1'){
					$('#dialog_msg').html('<div class="notify alert alert-danger"><b>Error: </b>Incorrect credentials provided</div>');
					setTimeout(function(){ $('.notify').hide(); }, 3000);
				}else{
					$('#dialog_msg').html('<div class="notify alert alert-danger"><b>Error: </b>Something went wrong. Please try again</div>');
					setTimeout(function(){ $('.notify').hide(); }, 3000);
				}
				// console.log('hello'+htm);
				

			}
		});

		
	});

});