<?php


// function which calls the required method from the $controller and $action variable obtained in index.php
function call($controller, $action) {
  require_once('controllers/' . $controller . '_controller.php');

  switch($controller) {
    case 'questions':
    require_once('models/question.php');
    require_once('models/subject.php');
    $controller = new QuestionsController();
    break;
    case 'user':
    require_once('models/user.php');
    $controller = new UserController();
  }
// calling the controller action
  $controller->{ $action }();
}

  // This array manages which values for controller and method will be accepted
  // all new controller and actions will have to be sorta registered here
$controllers = array('questions' => ['index','add_question','question_handler', 'explanation_handler',],
 'user' => ['login', 'login_handler']);

if (array_key_exists($controller, $controllers)) {
  if (in_array($action, $controllers[$controller])) {
    call($controller, $action);
  } else {
              // call error page
    call('pages', 'error');
  }
} else {
  call('pages', 'error');
}
?>