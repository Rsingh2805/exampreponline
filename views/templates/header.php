  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if(isset($title)){echo $title;} ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <script src="views/scripts/js_lib/jquery.js"></script>
    <?php
      if(isset($header)){
    echo $header;
    } ?>
  </head>
  <body>
    <header>
      <div class="container">
        <?php include 'views/templates/nav.php';?>
      </div>
      


    </header>
    <main role="main" class="container" style="margin-top: 60px;">