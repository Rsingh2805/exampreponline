<?php
// Configuration file for codemirror
$codemirror_header = '';
$codemirror_footer = '';
$codemirror_header .= '<link rel="stylesheet" href="views/libraries/codemirror/codemirror-5.31.0/lib/codemirror.css">
';
$codemirror_header .= '<script src="views/libraries/codemirror/codemirror-5.31.0/lib/codemirror.js"></script>';
$codemirror_header .= '<script src="views/libraries/codemirror/codemirror-5.31.0/mode/javascript/javascript.js"></script>';
$codemirror_header .= '
<style type="text/css">
.CodeMirror {
	/* Bootstrap Settings */
	box-sizing: border-box;
	margin: 0;
	font: inherit;
	overflow: auto;
	font-family: inherit;
	display: block;
	width: 100%;
	font-size: 80%;
	line-height: 1.42857143;
	color: #555;
	background-color: #fff;
	background-image: none;
	border: 1px solid #ccc;
	border-radius: 4px;
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
	/* Code Mirror Settings */
	font-family: monospace;
	position: relative;
	overflow: hidden;
	height: auto;
}

.CodeMirror-focused {
	/* Bootstrap Settings */
	border-color: #66afe9;
	outline: 0;
	box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
	transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}

</style>';



$codemirror_footer.= '<script src="views/libraries/codemirror/codemirror.js"></script>';







?>