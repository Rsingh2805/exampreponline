<?php

// questions class
abstract class Question_base{
	public $question;
	public $solution;
	public $id;

	function __construct($question_text, $question_solution, $question_id){
		$this->question = $question_text;
		$this->solution = $question_solution;
		$this->id = $question_id;
	}

	public static function decode_data($json_data){
		$data = json_decode($json_data);
		$final = "";
		foreach ((array)$data as $item) {
			if($item->type=='simple'){
				$final.= '<textarea class="form-control" data-type="simple" name="simple[]" readonly=true>'.$item->value .'</textarea>';
			}else if($item->type=='codemirror'){
				$final.= '<textarea class="code" data-type="codemirror">'.$item->value .'</textarea>';
			}else if($item->type=='ckeditor'){
				$final.= '<textarea class="ckeditor" name="ckeditor" data-type="ckeditor">'.$item->value .'</textarea>';
			}
		}
		return $final;
	}

	public static function decode_solution($json_data, $id){
		$data = json_decode($json_data);
		$option = $data->option;
		if($option!='a'){
			$acheck = 'disabled';
		}else{
			$acheck = 'checked';
		}
		if($option!='b'){
			$bcheck = 'disabled';
		}else{
			$bcheck = 'checked';
		}
		if($option!='c'){
			$ccheck = 'disabled';
		}else{
			$ccheck = 'checked';
		}
		if($option!='d'){
			$dcheck = 'disabled';
		}else{
			$dcheck = 'checked';
		}
		$string = '<div class="row form-group">'.
		'<div class="form-inline col-md-6">'.
		'<input type="radio" name="'.$id.'" value="a" '.$acheck.' readonly=true>&nbsp;'.
		'<textarea name="option1" id="sol_option_1" class="form-control form-control-sm" placeholder="Enter option 1" required="true" readonly=true>'.$data->option1.'</textarea>'.
		'</div>'.
		'<div class="form-inline col-md-6">'.
		'<input type="radio" name="'.$id.'" value="b" '.$bcheck.' readonly=true>&nbsp;'.
		'<textarea name="option2" id="sol_option_2" class="form-control form-control-sm" placeholder="Enter option 2" required="true" readonly=true>'.$data->option2.'</textarea>'.
		'</div>'.
		'</div>'.
		'<div class="row form-group">'.
		'<div class="form-inline col-md-6">'.
		'<input type="radio" name="'.$id.'" value="c" '.$ccheck.' readonly=true>&nbsp;'.
		'<textarea name="option3" id="sol_option_3" class="form-control form-control-sm" placeholder="Enter option 3" required="true" readonly=true>'.$data->option3.'</textarea>'.
		'</div>'.
		'<div class="form-inline col-md-6">'.
		'<input type="radio" name="'.$id.'" value="d" '.$dcheck.' readonly=true>&nbsp;'.
		'<textarea name="option4" id="sol_option_4" class="form-control form-control-sm" placeholder="Enter option 4" required="true" readonly=true>'.$data->option4.'</textarea>'.
		'</div>'.
		'</div>';
		// print_r($acheck.'b'.$bcheck.'c'.$ccheck.'d'.$dcheck);
		return $string;
	}

	abstract protected function show();
	
};


// class for4 textual questions
class Textual extends Question_base{

	public function show(){
		$string= '<li class="list-group-item">'.
		'<div class="container-fluid">'.
		'<small>Question:</small>'.
		'<h4>'. $this->question. '</h4>'.
		'</div>'.
		'<div class="container-fluid">'.
		'<small>Solution</small>'.
		'<textarea class="code">'. $this->solution. '</textarea>'.
		'</div>'.
		'<div class="container-fluid">'.
		'<small>Explanation:</small>'.
		'<pre>'. $this->explanation . '</pre>'.
		'</div>'.
		'</li>';
		return $string; 
	}
};



// class for mixed type4 problems
class Other extends Question_base{

	public function show(){
		$solution = Question_base::decode_data($this->solution);
		// $solution = json_decode($this->solution);
		// $solution_part = "";
		// foreach ((array)$solution as $item) {
		// 	if($item->type=='simple'){
		// 		$solution_part.= $item->value .PHP_EOL;
		// 	}else if($item->type=='codemirror'){
		// 		$solution_part.= '<textarea class="code">'.$item->value .'</textarea>';
		// 	}else if($item->type=='ckeditor'){
		// 		$solution_part.= '<textarea class="ckeditor" name="ckeditor">'.$item->value .'</textarea>';
		// 	}
		// }
		$string= '<li class="list-group-item">'.
		'<div class="container-fluid">'.
		'<small>Question:</small>'.
		'<h4>'. $this->question. '</h4>'.
		'</div>'.
		'<div class="container-fluid">'.
		'<small>Solution</small>'.
		'<div class="well">'.$solution. '</div>'.
		'</div>'.
		'<div class="container-fluid">'.
		'<small>Explanation:</small>'.
		'<pre>'. $this->explanation . '</pre>'.
		'</div>'.
		'</li>';
		return $string; 
	}
};



// class for mcq questions
class Mcq extends Question_base{
	

	public function show(){
		$question = Question_base::decode_data($this->question);
		// $explanation = Question_base::decode_data($this->explanation);
		$solution = Question_base::decode_solution($this->solution, $this->id);

		$string= '<li class="list-group-item">'.
		'<div class="container-fluid">'.
		'<small>Question:</small>'.
		'<h4>'. $question. '</h4>'.
		'</div>'.
		'<div class="container-fluid">'.
		'<small>Solution</small>'.
		'<div class="container well">'. $solution. '</div>'.
		'</div>'.

		'<div id="accordion" role="tablist">'.
		'<div class="card">'.
		'<div class="card-header" role="tab" id="heading'.$this->id.'">'.
		'<h5 class="mb-0">'.
		'<a class="show_explanation" data-toggle="collapse" href="#collapse'.$this->id.'" role="button" aria-expanded="true" aria-controls="collapse'.$this->id.'">'.
		'View Explanation'.
		'</a>'.
		'</h5>'.
		'</div>'.

		'<div id="collapse'.$this->id.'" class="collapse" role="tabpanel" aria-labelledby="heading'.$this->id.'" data-parent="#accordion">'.
		'<div class="card-body">'.
		'<div class="container-fluid">'.
		'<form action="?controller=questions&action=explanation_handler" class="explanation_form"><small>Explanation:</small>'.
		''.
		'</form>'.
		'<button class="edit_explanation">Edit</button></div>'.
		'</div>'.
		'</div>'.
		'</div>'.

		'</div>'.

		'<button type="button" class="btn btn-primary report" data-toggle="modal" data-target="#reportModal" id="'.$this->id.'">Report</button>'.
		'</li>';
		return $string; 
	}
};

// class for logical questions
class Logical extends Question_base{

	public function show(){
		$string= '<li class="list-group-item">'.
		'<div class="container-fluid">'.
		'<small>Question:</small>'.
		'<h4>'. $this->question. '</h4>'.
		'</div>'.
		'<div class="container-fluid">'.
		'<small>Solution</small>'.
		'<text><pre>'. $this->solution. '</pre></text>'.
		'</div>'.
		'<div class="container-fluid">'.
		'<small>Explanation:</small>'.
		'<textarea class="ckeditor" name="ckeditor">'. htmlspecialchars_decode($this->explanation) . '</textarea>'.
		'<button>Edit</button>'.
		'</div>'.
		'</li>';
		return $string; 
	}
};


?>
