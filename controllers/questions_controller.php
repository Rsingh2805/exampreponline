<?php 
class QuestionsController{

	public function __construct(){
		session_start();
		if(empty($_SESSION['user_id'])){
			header('Location: ./?controller=user&action=login');
		}
		require_once('DAO/DB.php');
		// $db = new DB();
	}

// index page showingthe list of all questions
	public function index(){
		$db = new DB();
		$questions = $db->getRows('questions');
		foreach ($questions as $question) {
			$list[$question['id']] = new Question($question['id'], $question['title'], $question['solution'], $question['user_id'], $question['type'],  $question['subject_id']);
			# code...
		}
		// create a list of question objects
		$questions = $list;
		// $questions = Question::all();
		require_once('views/apps/studymaterial/question_display/views/index.php');
	}


// renders the add question page
	public function add_question(){
		$db = new DB();
		$subjects = $db->getRows('subject');
		foreach($subjects as $subject) {
			$list[] = new subject($subject['sub_id'], $subject['title']);
		}
		$subjects = $list;
		// $subjects = Subject::all();
		require_once('views/apps/studymaterial/question_addition/views/add_question.php');
	}


// conrtols the CRUD operations on the questions
	public function question_handler(){
		if(isset($_REQUEST['req_type']) && !empty($_REQUEST['req_type'])){
			$req_type = $_REQUEST['req_type'];
			switch($req_type){


				case "view":
				$db = new DB();
				if(!empty($_POST['id'])&& isset($_POST['id'])){
					$conditions = array('where'=>array('id' => $_POST['id']));
					$records = $db->getRows('questions', $conditions);
				}else{
					$records = $db->getRows('questions');
				}
				if($records){
					$data['records'] = $records;
					$data['status'] = 'OK';
				}else{
					$data['records'] = array();
					$data['status'] = 'ERR';
				}
				echo json_encode($data);
				break;



				case "add":
				if(!empty($_POST)){
					if($_POST['question_type']=='textual'){
						$question_solution = $_POST['question_solution_code'];
					}else if($_POST['question_type']=='mcq'){
						$question = $_POST['question'];
						$explanation = $_POST['explanation'];
						$question_solution = $_POST['solution'];
					}else if($_POST['question_type']=='logical'){
						$question_solution = $_POST['add_question_page_ta'];
					}else if($_POST['question_type']=='other'){
						$question_solution = $_POST['question_solution_code'];
					};
					if(!isset($question)){
						$question = $_POST['question_text'];
					}
					if(!isset($explanation)){
						$explanation = $_POST['question_explanation'];
					}
					$questionData = array(
						'title' => $question,
						// 'explanation' => $explanation,
						'subject_id' => $_POST['question_subject_id'],
						'type' => $_POST['question_type'],
						'user_id' => $_SESSION['user_id'],
						'solution'=>$question_solution,
					);
					// $data = Question::add_question($questionData);
					$db = new DB();
					$insert_question = $db->insert('questions',$questionData);
					$id = $insert_question['id'];
					$explanationData = array(
						'ques_id' => $id,
						'explanation' => $explanation,
						'user_id' =>$_SESSION['user_id'],
						'credits' => '',
					);
					$insert_explanation = $db->insert('explanation', $explanationData);
					if($insert_question&&$insert_explanation){
						$data['data'] = $insert_question;
						$data['status'] = 'OK';
						$data['msg'] = '0';
					}else{
						$data['status'] = 'ERR';
						$data['msg'] = '1';
					}
   // return $data;
					// $insert = $db->insert('questions',$questionData);
					// if($insert){
					// 	$data['data'] = $insert;
					// 	$data['status'] = 'OK';
					// 	$data['msg'] = '0';
					// }else{
					// 	$data['status'] = 'ERR';
					// 	$data['msg'] = '1';
					// }
				}else{
					$data['status'] = 'ERR';
					$data['msg'] = '1';
				}

				echo json_encode($data);
				break;


				case "edit":
				if(!empty($_POST)){
					$questionData = array();
					$explanation = $_POST['explanation'];
					$solution = $_POST['solution'];
					$question = $_POST['question'];
					if(isset($explanation)){
						$questionData['explanation'] = $explanation;
					}
					if(isset($solution)){
						$questionData['solution'] = $solution;
					}
					if(isset($question)){
						$questionData['title'] = $question;
					}
					$condition = array('id' => $_POST['id']);
					$db = new DB();
					$update = $db->update('questions',$questionData,$condition);
					if($update){
						$data['status'] = 'OK';
						$data['msg'] = 'Question data has been updated successfully.';
					}else{
						$data['status'] = 'ERR';
						$data['msg'] = 'Some problem occurred, please try again.';
					}
				}else{
					$data['status'] = 'ERR';
					$data['msg'] = 'Some problem occurred 2, please try again.';
				}
				echo json_encode($data);
				break;


				case "delete":
				if(!empty($_POST['id'])){
					$condition = array('id' => $_POST['id']);
					$delete = $db->delete('questions',$condition);
					if($delete){
						$data['status'] = 'OK';
						$data['msg'] = 'User data has been deleted successfully.';
					}else{
						$data['status'] = 'ERR';
						$data['msg'] = 'Some problem occurred, please try again.';
					}
				}else{
					$data['status'] = 'ERR';
					$data['msg'] = 'Some problem occurred, please try again.';
				}
				echo json_encode($data);
				break;
				default:
				echo '{"status":"INVALID"}';
			}
		}
	}


	public function explanation_handler(){
		if(isset($_REQUEST['req_type']) && !empty($_REQUEST['req_type'])){
			$req_type = $_REQUEST['req_type'];
			switch($req_type){


				case "view":
				$db = new DB();
				if(!empty($_POST['id'])&& isset($_POST['id'])){
					$conditions = array('where'=>array('id' => $_POST['id']));
					$records = $db->getRows('explanation', $conditions);
				}else{
					$records = $db->getRows('explanation');
				}
				if($records){
					$data['records'] = $records;
					$data['status'] = 'OK';
				}else{
					$data['records'] = array();
					$data['status'] = 'ERR';
				}
				echo json_encode($data);
				break;



				case "add":
				if(!empty($_POST)){
					if($_POST['question_type']=='textual'){
						$question_solution = $_POST['question_solution_code'];
					}else if($_POST['question_type']=='mcq'){
						$question = $_POST['question'];
						$explanation = $_POST['explanation'];
						$question_solution = $_POST['solution'];
					}else if($_POST['question_type']=='logical'){
						$question_solution = $_POST['add_question_page_ta'];
					}else if($_POST['question_type']=='other'){
						$question_solution = $_POST['question_solution_code'];
					};
					if(!isset($question)){
						$question = $_POST['question_text'];
					}
					if(!isset($explanation)){
						$explanation = $_POST['question_explanation'];
					}
					$questionData = array(
						'title' => $question,
						'explanation' => $explanation,
						'subject_id' => $_POST['question_subject_id'],
						'type' => $_POST['question_type'],
						'user_id' => $_SESSION['user_id'],
						'solution'=>$question_solution,
					);
					// $data = Question::add_question($questionData);
					$db = new DB();
					$insert = $db->insert('questions',$questionData);
					$id = $insert['data']['id'];
					$explanationData = array(
						'ques_id' => $id,
						'explanation' => $explanation,
						'user_id' =>$_SESSION['user_id'],
						'credits' => '',
					);
					$insert2 = $db->insert('explanation', $explanationData);
					if($insert2){
						$data['data'] = $insert2;
						$data['status'] = 'OK';
						$data['msg'] = '0';
					}else{
						$data['status'] = 'ERR';
						$data['msg'] = '1';
					}
   // return $data;
					// $insert = $db->insert('questions',$questionData);
					// if($insert){
					// 	$data['data'] = $insert;
					// 	$data['status'] = 'OK';
					// 	$data['msg'] = '0';
					// }else{
					// 	$data['status'] = 'ERR';
					// 	$data['msg'] = '1';
					// }
				}else{
					$data['status'] = 'ERR';
					$data['msg'] = '1';
				}

				echo json_encode($data);
				break;


				case "edit":
				if(!empty($_POST)){
					$questionData = array();
					$explanation = $_POST['explanation'];
					if(isset($explanation)){
						$questionData['explanation'] = $explanation;
					}
					$condition = array('ques_id' => $_POST['ques_id']);
					$db = new DB();
					$update = $db->update('explanation',$questionData,$condition);
					if($update){
						$data['status'] = 'OK';
						$data['msg'] = 'Question data has been updated successfully.';
					}else{
						$data['status'] = 'ERR';
						$data['msg'] = 'Some problem occurred, please try again.';
					}
				}else{
					$data['status'] = 'ERR';
					$data['msg'] = 'Some problem occurred 2, please try again.';
				}
				echo json_encode($data);
				break;


				case "delete":
				if(!empty($_POST['id'])){
					$condition = array('id' => $_POST['id']);
					$delete = $db->delete('questions',$condition);
					if($delete){
						$data['status'] = 'OK';
						$data['msg'] = 'User data has been deleted successfully.';
					}else{
						$data['status'] = 'ERR';
						$data['msg'] = 'Some problem occurred, please try again.';
					}
				}else{
					$data['status'] = 'ERR';
					$data['msg'] = 'Some problem occurred, please try again.';
				}
				echo json_encode($data);
				break;
				default:
				echo '{"status":"INVALID"}';
			}
		}
	}



}


?>