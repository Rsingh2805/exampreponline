<?php
  class subject {
    // we define 3 attributes
    // they are public so that we can access them using $subject->author directly
    public $id;
    public $title;
    
    public function __construct($id, $title) {
      $this->id      = $id;
      $this->title  = $title;
    }

    // public static function all() {
    //    $list = [];
    //    $db = Db::getInstance();
    //    $req = $db->query('SELECT * FROM subject');

    //    // we create a list of subject objects from the database results
    //    foreach($req->fetchAll() as $subject) {
    //      $list[] = new subject($subject['sub_id'], $subject['title']);
    //    }
    //    return $list;
    // }

    //  public static function find($id) {
    //    $db = Db::getInstance();
    //    // we make sure $id is an integer
    //    $id = intval($id);
    //    $req = $db->prepare('SELECT * FROM subject WHERE id = :id');
    //    // the query was prepared, now we replace :id with our actual $id value
    //    $req->execute(array('id' => $id));
    //    $subject = $req->fetch();

    //    return new subject($subject['sub_id'], $subject['title']);
    //  }
  }
?>
