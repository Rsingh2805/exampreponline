<?php
class Question {
    // we define 3 attributes
    // they are public so that we can access them using $question->author directly
  public $id;
  public $title;
  // public $explanation;
  public $solution;
  public $user_id;
  public $type;
  public $subject_id;

  public function __construct($id, $title, $solution, $user_id, $type, $subject_id) {
    $this->id      = $id;
    $this->title  = $title;
    // $this->explanation = $explanation;
    $this->solution      = $solution;
    $this->user_id = $user_id;
    $this->type = $type;
    $this->subject_id = $subject_id;
  }

 //  public static function all() {
 //   $list = [];
 //   $db = Db::getInstance();
 //   $req = $db->query('SELECT * FROM questions');

 //       // we create a list of question objects from the database results
 //   foreach($req->fetchAll() as $question) {
 //     $list[] = new Question($question['id'], $question['title'], $question['explanation'], $question['solution'], $question['user_id'], $question['type'],  $question['subject_id']);
 //   }
 //   return $list;
 // }

 // public static function find($id) {
 //   $db = Db::getInstance();
 //       // we make sure $id is an integer
 //   $id = intval($id);
 //   $req = $db->prepare('SELECT * FROM questions WHERE id = :id');
 //       // the query was prepared, now we replace :id with our actual $id value
 //   $req->execute(array('id' => $id));
 //   $question = $req->fetch();

 //   return new Question($question['id'], $question['title'], $question['explanation'], $question['solution'], $question['user_id'], $question['type'],  $question['subject_id']);
 // }


 // public static function add_question($binddata){
 //   $db = new DB();
 //   $insert = $db->insert('questions',$binddata);
 //   if($insert){
 //     $data['data'] = $insert;
 //     $data['status'] = 'OK';
 //     $data['msg'] = '0';
 //   }else{
 //     $data['status'] = 'ERR';
 //     $data['msg'] = '1';
 //   }
 //   return $data;
 // }
}
?>
