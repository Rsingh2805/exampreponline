<?php

// $controller = the controller name
// $action = the name of the controller method
  if (isset($_GET['controller']) && isset($_GET['action'])) {
    $controller = $_GET['controller'];
    $action     = $_GET['action'];
  } else {
    $controller = 'questions';
    $action     = 'index';
  }
  
  require_once('views/templates/routes.php');
?>