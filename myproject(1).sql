-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 11, 2018 at 02:46 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `solution` text NOT NULL,
  `explanation` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `title`, `solution`, `explanation`, `user_id`, `type`, `subject_id`) VALUES
(58, '[{"type":"simple","value":"How are you"},{"type":"codemirror","value":"Check"}]', 'a. Really\nb. \nc. \nd. \nThe correct answer is a', '[{"type":"simple","value":"is that so"},{"type":"codemirror","value":"Check"}]', 1, 'mcq', 1),
(59, 'Hi ', 'how are you	I', 'I am fine', 1, 'textual', 1),
(66, '[{"type":"simple","value":""}]', 'a. \nb. \nc. \nd. \nThe correct answer is ', '[{"type":"simple","value":""}]', 1, 'mcq', 1),
(67, '[{"type":"simple","value":""}]', 'a. \nb. \nc. \nd. \nThe correct answer is ', '[{"type":"simple","value":""}]', 1, 'mcq', 1),
(68, '[{"type":"simple","value":""}]', 'a. \nb. \nc. \nd. \nThe correct answer is ', '[{"type":"simple","value":""}]', 1, 'mcq', 1),
(69, '[{"type":"simple","value":""}]', 'a. asd\nb. \nc. \nd. \nThe correct answer is ', '[{"type":"simple","value":""}]', 1, 'mcq', 1),
(70, 'sdfsdf', 'sdfsdf	sdf', 'xcvsdvsf', 1, 'textual', 1),
(71, '[{"type":"simple","value":"sdffsd"}]', 'a. abc\nb. asdklad\nc. asdasd\nd. asdkas\nThe correct answer is b', '[{"type":"simple","value":""}]', 1, 'mcq', 1),
(72, 'Check', 'me', 'out', 1, 'textual', 1),
(73, 'Chech', 'akdl', 'askldas', 1, 'textual', 1),
(74, '[{"type":"simple","value":"Add question"}]', 'a. aslkd\nb. sldkfj\nc. klsdfj\nd. klsdjf\nThe correct answer is a', '[{"type":"simple","value":""}]', 1, 'mcq', 1),
(75, '', '', '', 1, 'textual', 1),
(76, '[{"type":"simple","value":"Add me"},{"type":"codemirror","value":"Check\\t"}]', 'a. hi\nb. hoe\nc. are\nd. toy\nThe correct answer is d', '[{"type":"simple","value":"sadkamsodmasd"}]', 1, 'mcq', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `sub_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`sub_id`, `title`) VALUES
(1, 'Competitive Coding'),
(2, 'Android Programming'),
(3, 'Java'),
(4, 'CPP'),
(5, 'Web Development');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `password`, `email`, `username`) VALUES
(1, 'Admin 1', 'fe01ce2a7fbac8fafaed7c982a04e229', 'admin123@gmail.com', 'admin1'),
(2, 'Admin 2', 'fe01ce2a7fbac8fafaed7c982a04e229', 'admin321@gmail.com', 'admin2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
